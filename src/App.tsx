import { NavBar } from './Components/NavBar';
import 'bootstrap/dist/css/bootstrap.css';
import { ColdOrWarm } from './Pages/ColdOrWarm';
import { Home } from './Pages/Home';
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";


function App() {

  return (
    <>
      <Router>
        <NavBar/>
        <Routes>
          <Route path="/" element={<Home/>} />
        </Routes>
        <Routes>
          <Route path="/coldOrWarm" element={<ColdOrWarm/>} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
