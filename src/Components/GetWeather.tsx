import axios from "axios"
import { useState } from "react"



export interface cityCredentials {
    name?: string,
    id: number,
    country: string
    main: {
        temp: number,
    }
}

//this was just to see what elements I needed for the exercise

export function GetWeather() {
    const [data, setData] = useState<cityCredentials | undefined>()
    const [location, setLocation] = useState('')
    const url = `https://api.openweathermap.org/data/2.5/weather?q=${location}&appid=98b7465353d383f3d0f3bc4a284a48ae&units=metric`
    // const [weather, setWeather] = useState()


    const inputCity = async (event: any) => {
        if (event.key === 'Enter') {
            const response = await axios.get(url)
            setData(response.data)
            // setWeather(response.data?.main.temp)
        }
    }


    return (
        <div>
            <input
                value={location}
                onChange={event => setLocation(event.target.value)}
                placeholder="Enter a City"
                onKeyDown={inputCity}
                type="text" />


        </div>
    )
}