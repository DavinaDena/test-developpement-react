import { Link } from "react-router-dom";


export function NavBar(value: any) {

    return (
        <div>
            <nav className="navbar navbar-expand navbar-light bg-light">
                <div className="container-fluid">

                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <Link className="nav-link " to="/">Home</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link " to="/coldOrWarm">Cold Or Warm</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    )
}