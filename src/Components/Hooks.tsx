import axios from "axios"
import { useEffect, useState } from "react"



export function useGetWeather(value: string):[any, Function] {

    //I use a useState to get as pass the value as props
    const [location, setLocation] = useState(value)
    //created a variable to insert the link
    const url = `https://api.openweathermap.org/data/2.5/weather?q=${value}&appid=98b7465353d383f3d0f3bc4a284a48ae&units=metric`

    //Made an async function to wait for the request to the server (link)
    async function getWeather() {
        const response = await axios.get(url)
        //and stored it on the setLocation
        setLocation(response.data)
    }

    //Used an useEffect to launch the function when the value changes
    //thats why he makes a request at every letter that I write
    useEffect(() => {
        getWeather()
    }, [value])
    

    //return the values and the function
    return [location,  getWeather]
}