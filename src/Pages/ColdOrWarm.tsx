import { useState } from "react"
import { useGetWeather } from "../Components/Hooks"


export function ColdOrWarm() {
    const [city, setCity] = useState('')
    const [temp] = useGetWeather(city)

    return (
        <>
            <div className="container">
                <div className="row">
                    <div className="d-flex justify-content-center">


                        <div className="input-group mb-3">
                            <input type="text" className="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
                                value={city}
                                onChange={event => setCity(event.target.value)}
                                placeholder="Enter a City" />
                        </div>
                    </div>
                </div>
            </div>


            {temp && temp?.main.temp <= 15 &&

                <div className=" vh-100 container-fluid coldBackground">
                    <div className="row">
                        <div className="d-flex justify-content-center pt-5 mt-5">
                            <h1 className="pt-5 mt-5 text-white">Il fait froid</h1>
                        </div>

                    </div>
                </div>
            }

            {temp && temp?.main.temp > 15 &&
                <div className=" vh-100 container-fluid warmBackground">
                    <div className="row">
                        <div className="d-flex justify-content-center pt-5 mt-5">
                            <h1 className="pt-5 mt-5 text-white">Il fait chaud</h1>
                        </div>

                    </div>
                </div>
            }



        </>

    )
}


