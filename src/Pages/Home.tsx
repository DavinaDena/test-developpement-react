import { useState } from "react";
import { useGetWeather } from "../Components/Hooks";
import { Info } from "../Components/Info";

export function Home() {
    //used a use state to recover and store the inserted city
    const [city, setCity] = useState('')
    //declared that the value that I passed as props is the inserted city
    const [temp] = useGetWeather(city)

    function handleChange(event: any) {
        return setCity(event.target.value)
    }


    return (

        <div className="container-fluid background vh-100 ">
            <div className="row">
                <div className="d-flex justify-content-center p-5">
                    <div className="input-group mb-3">
                        <input type="text" className="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
                            value={city}
                            onChange={handleChange}
                            placeholder="Enter a City" />
                    </div>
            
                </div>

                {temp &&
                    <Info city={temp} />
                }

            </div>
        </div>


    )
}




